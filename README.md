# Rackit

Parse and manipulate VCV Rack patch files

_(( This project is in very early stage development ))_

Rackit is a tool for parsing and manipulating VCV Rack patch files:

![Graph of default patch](TestPatches/DefaultPatch.vcv.gv.png "Rack built-in default patch")

Note that graphing a patch like this requires Graphviz and a python module to be installed:
        https://graphviz.org/download/
        https://pypi.org/project/graphviz/

The graphing generally works with v1.x _or_ v0.6 files, though, in case you want to look at and try to re-create old patches

Run `rackit -h` or `rackit - CMD -h` for help

**WARNING:** All commands that output to a file, though they require specifying the output, _WILL OVERWRITE WITHOUT PROMPTING_


Notes
-----
- The ultimate goal of this is to make composing patches from other patches easier and to streamline midi-mapping

- maybe also support MIDI-CAT? https://github.com/stoermelder/vcvrack-packone/blob/v1/docs/MidiCat.md

- STRIP module implements load/save of "strips" of modules: https://github.com/stoermelder/vcvrack-packone/blob/v1/docs/Strip.md
    - Very cool, but limited


Test Patches
------------
- LittleRuckus: https://patchstorage.com/little-ruckus-make-noise-0-coast-emulation-all-free-modules-2/
- Omri-Coast: https://patchstorage.com/lets-build-the-make-noise-o-coast-in-vcv-rack/
- MM_Realtime_Chariots: https://patchstorage.com/mm-realtime-chariots/  (_huge_ patch!  note: other assets _not_ copied in)
- OmriKrell-v0.6: https://patchstorage.com/lets-build-a-krell-patch-in-vcv-rack/  (Note: Rack v0.6 patch)


To Do
-----
- _much_ documentation
- safeguards e.g. for possibly destructive operations
    - DONE: move to `rackit PATCH command OUTFILE|DIR|- [ARGS]`
        - DONE: `-` means work in-place or similar default
        - DONE: `DIR` uses basename of patch
    - version checks

- DONE: add argparse
    - DONE: option to just render .gv (use VS Code plugin to preview)
    - DONE: maybe subcommands to do different operations?
    - look at argparse.subparser more

- DONE: move test/example patches to a different folder

- parse patch
    - PUNT: "patch ls X" - show patch as tree starting from module X
    - DONE: "patch graph" patch to graphviz or somesuch?
    - parse midi map connections
        - and midi-cat?
    - `-s/--show FMT` to output as different formats
    - parse into the modules themselves for more info (filter out or retain blanks for example)
        - maybe a simple filter `-x/--exclude` option would be easer and more versitile?
        - maybe `-h/--hide-unpatched` (should this hide or not hide memos, though?)
    - maybe option for "label affinity" so it doesn't need gaps?
    - DONE: be able to map out v0.6 patches so they can be rebuilt in modern versions
        - DONE:"cables" were called "wires"
        - DONE: modules had no ID, maybe used index in the modules list?

- maybe _try_ to fix up v0.6 patches enough to _try_ opening in Rack v1

- `extract X[...]` - create a new patch file extracting modules X... and their connections
    - automatically include "contiguous" non-patched modules? (e.g. blanks)
        - maybe `Patch` should maintain an "unpatched modules" set for this sort of thing
    - cleanup midi maps
    - `extract --inverse` instead of `delete`
        - should handle automatic includes differently (maybe do the auto-adds after the inversion? - no that would leave orphaned memos that were only connected to the pre-inversion extraction)
        - delete `delete`
    - `-c/--compact` automatically compact after extraction

- `compact` - try to move modules closer together
    - based on starting position?  cable distance?
    - how to move toward the "origin"?
        - module entries have `"pos": [54,1]` - this is at 54 HP to the right, one row down from the origin
    - how to find the module width, _it doesn't seem to be in the entries_
        - NO: have to parse into modules themselves? - it's not in the plugin.json either!
        - can we produce a file with bad/overlapping positions and have Rack fix it?
    - slide to origin: take min(hp), min(row) and subtract those from each `pos`
    - naieve vertical: find empty rows and move lower modules up
    - PUNT: naieve horizontal: in each row slide modules toward origin by gap widths
        - YUP: probably screw up vertical proximity, may need heuristics
    - PUNT: better horizontal:
        - find widest sum-of-module-widths row and compact that
        - attempt to position modules in other rows between the positions of the modules they connect to, working out from the widest row
    - PUNT: is it possible to get layout from graphviz and translate that into complete reorg?
        - research other graph layout algorithems?

- "patch merge P1 P2[...]" - take some patch files and make a new one that combines them naievely, just offsetting module groups spatially and fixing up conflicting IDs
    - check version numbers, maybe recommend opening and saving both/all to "freshen" if they don't match
    - "knit" patches by declaring ID equivalence
        - LOTS of edge-cases to guard against (conflicting cable connections, modules aren't the same type, ...)
        - parse and print possible knit modules
        - "smart" suggestions based on similar chains of modules

- Stoermelder Glue
    - Graph as "attached memo"
    - "bring along" with extrations
        - need to "fix up"
    - could it be possible to label controls based on the midi map?

- midi-map Default & Omri-Coast and start building that into graph
    - and add reverb

- Magically add and update Midi-mapped controls to Rack patches
    - "add midi map P1 MP" - add or update midi mappings from MP
    - maybe this becomes just one use of "knit"?
    - may need ability to specify offsets for CC numbers

- Automatic "map notes" parsing/updating
    - Make a DSL for writing out a midi map, then parse and apply it
        - Hmm, needs human-readable control labels somehow
    - Update "map notes" when adding midi-map settings

- maybe also graphviz-to-patch?  (if there's a good WYSIWYG .gv editor)

- set up MapPatches dir
    - NO: map some basic fundamentals - extract mappings from working patches
    - NO: map some third party modules - extract instead


## Doneyard

- DONE: `delete X[...]` - create a new patch file minus modules X... and their connections
    - PUNT: and orphaned subgraphs?
    - PUNT: cleanup midi maps - move to `extract -i` and make that do the right thing
    - PUNT: also offer downstream/upstream, -x, etc? - fold-in as an option for `extract`?

- DONE: `extract X[...]` - create a new patch file extracting modules X... and their connections
    - PUNT: "extract X[...] N" - create a new patch file extracting N steps out from modules X...- just do downstream/upstream mode
        - DONE: beware of cycles (use sets of IDs to avoid)
        - DONE: separate "upstream" vs. "downstream" N?  "From X extract 5 steps downstream and 1 step of input into those"
            - DONE: build one first then add the other?  Downstream first?  Larger N first?
    - PUNT: if more than one X, show the module tree for each - use IDs to start
    - PUNT: ability to use names after the first to say "starting from 123 extract VCO, VCF, 32, ..."
    - PUNT: "extract X up to Y" could be useful for pulling sub-graphs out up to the point they go into the mixer or whatever - do this with `-x`
    - DONE: automatically include adjacent memo modules
    - DONE: `-p/--precise` disable all "automatically include" stuff
    - DONE: `-x/--exclude` - exclude also stops downstream/upstream walking

---

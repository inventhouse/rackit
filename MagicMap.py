#!/usr/bin/env python
# Copyright (c) 2019 Benjamin Holt -- MIT License

import json

def load_patch(patch):
    with open(patch, "rb") as f:
        p = json.load(f)
    return p


adsr_patch = "MapPatches/FundamentalADSR.vcv"
vco_patch = "MapPatches/FundamentalVCO-1.vcv"
example_patch = "ExamplePatch.vcv"
# print(json.dumps(m, indent=2))

###
